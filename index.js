const express = require('express');

const app = express();
const port = 3000;

app.get('/', (req, res) => {
  res.send('Bonjour');
});

app.get('/index.html', (req, res) => {
  res.send(`<!DOCTYPE html><html lang="fr"><head><title>420-3D2</title></head>
    <body><h1>Titre de la page</h1>Ceci est une page web.</body></html>`);
});

app.get('/date', (req, res) => {
  res.send(`Nous sommes le: ${new Date()}`);
});

app.listen(port, () => {
  console.log(`Cette application web peut maintenant recevoir des requêtes: http://localhost:${port}`);
});
